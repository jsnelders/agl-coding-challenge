﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace AGLCodeChallenge.Tests
{
    [TestClass]
    public class ServiceTests
    {
        [TestMethod]
        public void GetGenderSortedCatList_CorrectDatasetRetrievedAndProcessed_SortedListReturned()
        {
            string jsonToReturn = "[{\"name\":\"Bob\",\"gender\":\"Male\",\"age\":23,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"},{\"name\":\"Fido\",\"type\":\"Dog\"}]},{\"name\":\"Jennifer\",\"gender\":\"Female\",\"age\":18,\"pets\":[{\"name\":\"Garfield\",\"type\":\"Cat\"}]},{\"name\":\"Steve\",\"gender\":\"Male\",\"age\":45,\"pets\":null},{\"name\":\"Fred\",\"gender\":\"Male\",\"age\":40,\"pets\":[{\"name\":\"Tom\",\"type\":\"Cat\"},{\"name\":\"Max\",\"type\":\"Cat\"},{\"name\":\"Sam\",\"type\":\"Dog\"},{\"name\":\"Jim\",\"type\":\"Cat\"}]},{\"name\":\"Samantha\",\"gender\":\"Female\",\"age\":40,\"pets\":[{\"name\":\"Tabby\",\"type\":\"Cat\"}]},{\"name\":\"Alice\",\"gender\":\"Female\",\"age\":64,\"pets\":[{\"name\":\"Simba\",\"type\":\"Cat\"},{\"name\":\"Nemo\",\"type\":\"Fish\"}]}]";
            MockWebServiceClient mockWebService = new MockWebServiceClient(jsonToReturn);

            Config config = new Config();
            Service service = new Service(config, mockWebService);

            List<Models.PetNamesByGender> sortedCats = service.GetGenderSortedCatList();

            /*
                Expected:
                Male
                - Garfield
                - Jim
                - Max
                - Tom

                Female
                - Garfield
                - Simba
                - Tabby
             */

            Assert.AreEqual(2, sortedCats.Count);

            var male = sortedCats.Where(c => c.PersonGender.ToLower() == "Male".ToLower()).SingleOrDefault();
            Assert.IsNotNull(male);
            Assert.AreEqual("Garfield,Jim,Max,Tom", string.Join(",", male.PetNames));

            var female = sortedCats.Where(c => c.PersonGender.ToLower() == "Female".ToLower()).SingleOrDefault();
            Assert.IsNotNull(female);
            Assert.AreEqual("Garfield,Simba,Tabby", string.Join(",", female.PetNames));
        }


        [TestMethod]
        public void GetGenderSortedCatList_NoPeopleAvailable_ZeroLengthListReturned()
        {
            //TODO: Write tests
            throw new NotImplementedException();
        }


        [TestMethod]
        public void GetGenderSortedCatList_ErrorRetrievingData_ZeroLengthListReturned()
        {
            //TODO: Write tests
            throw new NotImplementedException();
        }


        [TestMethod]
        public void GetGenderSortedCatList_PersonInDataHasNoPets_ReturnOnlySortedListOfPeopleWithPets()
        {
            //TODO: Write tests
            throw new NotImplementedException();
        }

    }
}
