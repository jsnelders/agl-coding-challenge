﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGLCodeChallenge.Tests
{
    public class MockWebServiceClient : AGLCodeChallenge.IWebServiceClient
    {
        public MockWebServiceClient(string json)
        {
            jsonToReturn = json;
        }

        private string jsonToReturn;

        public string GetWebServiceData(string url)
        {
            //throw new NotImplementedException();
            return jsonToReturn;
        }
    }
}
