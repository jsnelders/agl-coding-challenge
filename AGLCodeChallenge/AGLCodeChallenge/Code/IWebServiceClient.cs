﻿namespace AGLCodeChallenge
{
    public interface IWebServiceClient
    {
        string GetWebServiceData(string url);
    }
}