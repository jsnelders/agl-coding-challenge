﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AGLCodeChallenge.Models;
using AGLCodeChallenge.Entities;

namespace AGLCodeChallenge
{
    public class Service
    {
        public Service(Config config, IWebServiceClient webServiceClient)
        {
            this.config = config;
            this.webServiceClient = webServiceClient;
        }
        private Config config;
        private IWebServiceClient webServiceClient;

        

        private List<Person> GetPeopleList(string peopleJson)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<List<Person>>(peopleJson);
        }


        public List<PetNamesByGender> GetGenderSortedCatList()
        {
            List<PetNamesByGender> returnList = new List<PetNamesByGender>();

            // Get data from the web service
            string peopleJson = webServiceClient.GetWebServiceData(config.WebServiceUrl);

            // Convert data to strong entity structure
            List<Person> people = this.GetPeopleList(peopleJson);

            // Start building return list. 
            // Generate complete list of genders.
            var distinctGenders = people.GroupBy(person => person.Gender).Select(g => g.First()).ToList();
            distinctGenders.ForEach(gender => returnList.Add(new PetNamesByGender() { PersonGender = gender.Gender }));

            // Ensure only people with pets
            people = people.Where(person => person.Pets != null).ToList();

            // Add cats to each gender
            foreach (Person person in people)
            {
                var cats = person.Pets.Where(pet => pet.Type.ToLower() == "Cat".ToLower());
                foreach (var cat in cats)
                {
                    returnList.Where(p => p.PersonGender == person.Gender).Single().PetNames.Add(cat.Name);
                }
            }

            // Within each gender, sort cats alphabetically.
            returnList.ForEach(item => item.PetNames = item.PetNames.OrderBy(pet => pet).ToList());

            return returnList;
        }
    }
}