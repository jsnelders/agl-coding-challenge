﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace AGLCodeChallenge
{
    public class WebServiceClient : IWebServiceClient
    {
        public string GetWebServiceData(string url)
        {
            string json = "";

            using (WebClient client = new WebClient())
            {
                try
                {
                    json = client.DownloadString(url);
                }
                catch (Exception ex)
                {
                    // Logging and handle exceptions gracefully as needed.
                }
            }

            return json;
        }
    }
}