﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLCodeChallenge
{
    public class Config
    {
        public Config()
        {
            this.WebServiceUrl = "http://agl-developer-test.azurewebsites.net/people.json";
        }
        public string WebServiceUrl { get; set; }
    }
}