﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AGLCodeChallenge.Models;
using AGLCodeChallenge.Entities;

namespace AGLCodeChallenge.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
            config = new Config();
            webServiceClient = new WebServiceClient();
            service = new Service(config, webServiceClient);
        }
        private Config config;
        private IWebServiceClient webServiceClient;
        private Service service;

        
        public ActionResult Index()
        {
            IndexModel model = new IndexModel();

            model.GenderSortedPets = service.GetGenderSortedCatList();

            return View(model);
        }
    }
}