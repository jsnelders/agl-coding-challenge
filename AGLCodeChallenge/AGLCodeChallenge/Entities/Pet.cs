﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLCodeChallenge.Entities
{
    public class Pet
    {
        public Pet()
        {
            this.Name = "";
            this.Type = "";
        }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}