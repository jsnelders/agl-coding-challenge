﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLCodeChallenge.Entities
{
    public class Person
    {
        public Person()
        {
            this.Name = "";
            this.Gender = "";
            this.Age = 0;
            this.Pets = new List<Pet>();
        }

        public string Name { get; set; }

        public string Gender { get; set; }

        public int Age { get; set; }

        public List<Pet> Pets { get; set; }
    }
}