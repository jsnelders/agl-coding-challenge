﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLCodeChallenge.Models
{
    /// <summary>
    /// Represents all pet names for pets grouped by owner gender.
    /// </summary>
    public class PetNamesByGender
    {
        public PetNamesByGender()
        {
            this.PersonGender = "";
            this.PetNames = new List<string>();
        }

        public string PersonGender { get; set; }

        public List<string> PetNames { get; set; }
    }
}