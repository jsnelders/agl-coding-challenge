﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLCodeChallenge.Models
{
    public class IndexModel
    {
        public IndexModel()
        {
            this.GenderSortedPets = new List<PetNamesByGender>();
        }

        public List<PetNamesByGender> GenderSortedPets { get; set; }
    }
}